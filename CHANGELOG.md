# Change Log

## 25/03/2022 v0.1.1

**validators**:

- ExtendedURLValidator
- URLDoesNotExistValidator
- non_whitespace
- non_python_keyword

**loading**:

- get_model
- is_model_registered

**forms.fields**:

- ExtendedURLField
